#include <stdio.h>

int main()
{
	float n1, n2;
	printf("Enter 2 floating point numbers: ");
	scanf("%f %f", &n1, &n2);
	float result = n1 * n2;
	printf("Multiplication of 2 numbers: %f\n", result);

	return 0;
}
