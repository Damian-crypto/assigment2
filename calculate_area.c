#include <stdio.h>

int main()
{
	float radii;
	printf("Enter the radius of the disk: ");
	scanf("%f", &radii);
	float area = 3.141 * radii * radii;
	printf("Area of the disk is: %f\n", area);

	return 0;
}
